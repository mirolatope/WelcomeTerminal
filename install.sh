#!/bin/bash

# Verify user permissions
if [ $USER != "root" ]; then
    echo "Permission denied ! Are you sure you're root ?"
    exit 1
fi

# Verify / install virtualenv
dpkg -s virtualenv 1>/dev/null
if [ $? -eq 1 ]; then
    echo "Missing virtualenv package. Installation in progress"
    sudo apt-get install -y virtualenv
    echo "Installation done"
fi

# Verify python3 installed
python3 -V 1>/dev/null
if [ $? -ne 0 ]; then
    echo "Missing python3 package ! (weird)"
    exit 1
fi

# Verify version >= 3.6.5
pythonVersion=$(python3 -V | grep -o "[0-9]\.[0-9]\.[0-9]" | sed -re 's/\.//g')
if [ $pythonVersion -lt 365 ]; then
    echo "Minimal python3 version required: 3.6.5 !"
    exit 1
fi

# Installation of the virtual env + required packages
script=`realpath $0`
scriptPath=`dirname $script`

echo 'Creation of the virtual environment folder'
sudo -u $SUDO_USER mkdir $scriptPath/venv

echo 'Initialization of the vend'
/usr/bin/virtualenv $scriptPath/venv/
echo 'Update group and owner of the venv'
chown -R $SUDO_USER:$SUDO_USER $scriptPath/venv

echo 'Source venv'
source $scriptPath/venv/bin/activate

echo 'Installation required packages for the project'
sudo -u $SUDO_USER $scriptPath/venv/bin/pip install -r $scriptPath/requirements.txt

echo 'Exit from venv'
deactivate

exit 0