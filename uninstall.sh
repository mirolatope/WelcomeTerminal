#!/bin/bash

# Verify user permissions
if [ $USER != "root" ]; then
    echo "Permission denied ! Are you sure you're root ?"
    exit 1
fi

location=$(pwd)

echo "Suppression of the virtual environment."
`rm -Rf $location/venv`
echo "Suppression done"