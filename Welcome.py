from Quote import QuoteDisplayer
from Quote import QuoteManager

def Main(category = 'inspire'):
    quote = QuoteManager.GetQuote(category)
    QuoteDisplayer.Display(quote)

if __name__ == "__main__":
    Main()