import json

def __GetQuoteInformationFromHttpResponse(httpResponse):
    if httpResponse is not None:
        data = json.loads(httpResponse)
        return data["contents"]["quotes"][0]
    else:
        return None

def __GetDateOfQuote(quoteInfo):
    date = None
    if 'date' in quoteInfo:
        date = quoteInfo['date']
    return date

if __name__ == '__main__':
    from Quote import Mockups
    print(__GetDateOfQuote(Mockups.quoteInfoMockup))