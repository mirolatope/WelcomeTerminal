import json
import os

QuoteInfoPath = os.path.dirname(os.path.abspath(__file__)) + '/QuoteOfDay.json'

def __SaveQuoteInformation(quoteInfo):
    global QuoteInfoPath
    with open(QuoteInfoPath, 'w') as f:
        json.dump(quoteInfo, f, indent=4)

def __ReadQuoteInformation():
    global QuoteInfoPath
    quoteInfo = None
    if os.path.exists(QuoteInfoPath):
        with open(QuoteInfoPath, 'r') as f:
            quoteInfo = json.load(f)
    return quoteInfo

if __name__ == "__main__":
    from Quote import Mockups
    __SaveQuoteInformation(Mockups.quoteInfoMockup)