import os

def __DisplayQuote(quote):
    terminalWidth = os.get_terminal_size().columns
    output = f'"{quote}"'
    print(output.center(terminalWidth))

def __DisplayAuthor(author):
    terminalWidth = os.get_terminal_size().columns
    output = f'\t-{author}'
    print(output.center(terminalWidth))

def Display(quoteInfo):
    if quoteInfo is not None:            
        quote = quoteInfo['quote']
        __DisplayQuote(quote)
        author = quoteInfo['author']
        __DisplayAuthor(author)

if __name__ == "__main__":
    quoteInfoMockup = {
        'quote': 'It is not the mountain we conquer, but ourselves.',
        'length': '49',
        'author': 'Edmund Hillary',
        'tags': ['inspire', 'life', 'mountaineering'],
        'category': 'inspire',
        'date': '2018-08-09',
        'permalink': 'https://theysaidso.com/quote/74qX3QA323z6d1ePUS_GfweF/930975-edmund-hillary-it-is-not-the-mountain-we-conquer-but-ourselves',
        'title': 'Inspiring Quote of the day',
        'background': 'https://theysaidso.com/img/bgs/man_on_the_mountain.jpg',
        'id': '74qX3QA323z6d1ePUS_GfweF',
    }
    
    Display(quoteInfoMockup)